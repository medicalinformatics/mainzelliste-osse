# Mainzelliste.OSSE

## General information
Mainzelliste (see <https://bitbucket.org/medicalinformatics/mainzelliste>) is a web-based patient list and pseudonymization service. It provides a RESTful interface for client applications like electronic data capture (EDC) systems.

Mainzelliste.OSSE includes additions made to the standard Mainzelliste distribution within the Open Source Registry System for Rare Diseases (OSSE, see <https://bitbucket.org/medicalinformatics/samply.edc.osse/wiki/Home>). These additions comprise mainly user forms adapted to the OSSE layout.

## Release notes

This project depends on Mainzelliste 1.7.0 and makes the following changes to the standard distribution:

- User forms are displayed in the OSSE look and feel.
- The application can be configured to register patients by location of registration and a locally unique pseudonym instead of identifying data. A configuration template for this case is included as `src/main/resources/mainzelliste-no-idat.conf.default`.

Please also see the release notes and changelog of the Mainzelliste base project.

## Build

In order to build Mainzelliste.OSSE, you also need to import the compatible version of [Mainzelliste](https://bitbucket.org/medinfo_mainz/mainzelliste) as stated in the release notes into your development environment or install it in your local Maven repository. Mainzelliste is imported in Mainzelliste.OSSE via the [WAR overlay mechanism](https://maven.apache.org/plugins/maven-war-plugin/overlays.html) of Maven.

Use Maven to build the jar:

```
mvn clean package
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.pseudonymisierung.mainzelliste</groupId>
    <artifactId>mainzelliste-osse</artifactId>
    <version>1.7.0</version>
</dependency>
```