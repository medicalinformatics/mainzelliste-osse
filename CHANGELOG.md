# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html), but version numbers are linked to the version of the Mainzelliste.

## [1.7.0] - 2019-06-12
### Changed
- Mainzelliste version 1.7.0

## [1.6.2] - 2019-05-15
### Changed
- Mainzelliste version 1.6.2

## [1.6.1] - 2017-11-08
### Changed
- Using Mainzelliste version 1.6.1

## [1.5.0] - 2015-12-22

First public source release. Depends on Mainzelliste 1.5.0 and makes the following changes to the standard distribution:

- User forms are displayed in the OSSE look and feel.
- The application can be configured to register patients by location of registration and a locally unique pseudonym instead of identifying data. A configuration template for this case is included as `src/main/resources/mainzelliste-no-idat.conf.default`.
