function fixDiv() {
    var $cache = $('#stickyButtonBar');

    var $pageheight = $('#patient').height() + $('#nav').height();
    var $stickyheight = $cache.height();
    var $stickyIsFixed = $cache.css('position')=='fixed';

    if($stickyIsFixed) {
        if ($pageheight + $stickyheight + 20 <= $(window).height()) {
            $('#stickyButtonBar').css({'position': 'relative', 'top': 'auto'});
        }
    } else {
        if ($pageheight > $(window).height()) {
            $('#stickyButtonBar').css({'position': 'fixed', 'bottom': '0px'});
        }
    }

    $(".sticky_bar").width($(".right").width() - 20);
}

(function() {
  var app;

  $(document).ready(function() {
    return app.init();
  });

  $(window).load(function() {

	     $(".sticky_bar").addClass("sticky_bottom");
		 $(".sticky_bar").width($(".right").width()-20);


	    //return app.init();

	  });
  
  app = {
    init: function() {
//      $("#episode-slider").slick({
//        slidesToScroll: 1,
//        centerMode: true,
//        infinite: false,
//        slide: 'a',
//        variableWidth: true
//      });
    	
//    	alert($(".sticky_bar"));
//    	alert($(".sticky_bar").length);
//    	alert($(".sticky_bar").offset());
    	
    	if($(".sticky_bar").length == 0) {
    	}
    	else
    	{
	      this.bind_events();
	      //return window.offset_top = $(".sticky_bar").offset().top;
    	}
    },
    bind_events: function() {
    	$(window).scroll(fixDiv);
    	$(window).resize(fixDiv);
    	fixDiv();

//      return $(window).scroll(function() {
//        //return app.sticky_nav_top();
//      });
    },
    sticky_nav_top: function() {
      if ($(window).scrollTop() >= window.offset_top) {
        $(".sticky_bar").addClass("sticky_top");
        return $(".sticky_spacer").show();
      } else {
        $(".sticky_bar").removeClass("sticky_top");
        return $(".sticky_spacer").hide();
      }
    }
  };

}).call(this);
