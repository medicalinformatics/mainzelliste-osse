<%@page import="de.pseudonymisierung.mainzelliste.Config"%>
<%@page import="java.util.ResourceBundle"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%
	ResourceBundle bundle = Config.instance.getResourceBundle(request);
%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<jsp:include page="cssJsIncludes.jsp"></jsp:include>
<title><%=bundle.getString("error")%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body class="index">
	<jsp:include page="header.jsp"></jsp:include>
	<div id="patient">
		<div class="container">
			<h2><%=bundle.getString("errorHasOccured")%></h2>
			<hr>
			<div class="general_documents">
				<div class="right col-xs-12">
					<div id="topFormPanel" class="panel panel-default">
						<div class="panel-heading">
							<h4><%=bundle.getString("errorMessage")%></h4>
						</div>
						<div class="panel-body">
							<p>${it.message}</p>
						</div>
					</div>
					<div style="position: fixed; bottom: 0px; width: 760px;"
						id="stickyButtonBar" class="sticky_bar sticky_bottom">
						<div class="actions">
							<a href="#" onclick="window.history.back()" id="cancel"
								class="btn btn-default"> <i class="fa fa-undo fa-fw"></i>
								<%=bundle.getString("back")%>
							</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

</body>
</html>
