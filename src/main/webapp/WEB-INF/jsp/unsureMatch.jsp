<%@page import="de.pseudonymisierung.mainzelliste.Config"%>
<%@page import="java.util.ResourceBundle"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	ResourceBundle bundle = Config.instance.getResourceBundle(request);
	// pass "language" parameter from URL if given (included in form URL below)
	String languageInUrl ="";
	if (request.getParameter("language") != null)
		languageInUrl = "&amp;language=" + request.getParameter("language");
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="cssJsIncludes.jsp"></jsp:include>
<title><%=bundle.getString("unsureCase")%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body class="index">
	<jsp:include page="header.jsp"></jsp:include>
	<div id="patient">
		<div class="container">
			<h2><%=bundle.getString("unsureCase")%></h2>
			<hr>

			<div class="general_documents">
				<div class="right col-xs-12">
					<form
						action="<%=request.getContextPath() %>/patients?mainzellisteApiVersion=${it.mainzellisteApiVersion}&amp;tokenId=${it.tokenId}<%=languageInUrl %>"
						method="post" id="form_person">
						<div id="topFormPanel" class="panel panel-default">
							<div class="panel-heading">
								<h4><%=bundle.getString("patientData")%></h4>
							</div>
							<div class="panel-body">
								<%=bundle.getString("unsureCaseText")%>
								<ul class="hinweisen_liste">
									<li><span class="blauer_text"><%=bundle.getString("unsureCaseInfoRevise")%></span></li>
									<li><span class="blauer_text"><%=bundle.getString("unsureCaseInfoConfirm")%></span></li>
									<li><span class="blauer_text"><%=bundle.getString("unsureCaseInfoSupport")%></span></li>
								</ul>
								<jsp:include page="patientFormElements.jsp"></jsp:include>
								<input type="hidden" name="sureness" value="true">
							</div>
						</div>
						<div
							style="position: fixed; bottom: 0px; top: auto; width: 760px;"
							id="stickyButtonBar" class="sticky_bar sticky_bottom">
							<div class="actions">
								<a href="#"
									onclick="document.getElementById('form_person').submit();"
									id="save" style="margin-right: 20px;" class="btn btn-success">
									<i class="fa fa-check fa-fw"></i> <%=bundle.getString("confirm")%>
								</a><a href="#" onclick="window.history.back()" id="cancel"
									class="btn btn-danger"> <i class="fa fa-undo fa-fw"></i> <%=bundle.getString("correct")%>
								</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>