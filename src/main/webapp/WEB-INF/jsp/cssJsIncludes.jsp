<link rel='stylesheet' type="text/css" 
	href='<%=request.getContextPath()%>/webjar/bootstrap/css/bootstrap.min.css'/>
	
<link rel='stylesheet' type="text/css"
	href='<%=request.getContextPath()%>/webjar/font-awesome/css/font-awesome-jsp.css'/>
	
<link rel='stylesheet' type="text/css"
	href='<%=request.getContextPath()%>/webjar/font-lato/css/font-lato-jsp.css'/>

<link rel='stylesheet' type="text/css"
	href='<%=request.getContextPath()%>/webjar/samply/css/buttons.css'/>

<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/static/css/nav.css">


<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/static/css/osse.css">

<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/static/css/pages.css">
	
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/static/css/media_queries.css">


<script type="text/javascript"
	src='<%=request.getContextPath()%>/webjar/jquery/js/jquery.min.js'></script>
	
<script type="text/javascript"
	src='<%=request.getContextPath()%>/webjar/bootstrap/js/bootstrap.min.js'></script>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/static/js/scripts.js"></script>


<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport"
	content="width=device-width, minimum-scale=1, maximum-scale=1">

