<%@page import="de.pseudonymisierung.mainzelliste.Config"%>
<%@page import="java.util.ResourceBundle"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	ResourceBundle bundle = Config.instance.getResourceBundle(request);
	// pass "language" parameter from URL if given (included in form URL below)
	String languageInUrl ="";
	if (request.getParameter("language") != null)
		languageInUrl = "&amp;language=" + request.getParameter("language");
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="cssJsIncludes.jsp"></jsp:include>
<title><%=bundle.getString("editPatientTitle")%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body class="index">
	<jsp:include page="header.jsp"></jsp:include>
	<div id="patient">
		<div class="container">
			<h2><%=bundle.getString("editPatientTitle")%></h2>
			<hr>

			<div class="general_documents">
				<div class="right col-xs-12">
						<form
							action="<%=request.getContextPath() %>/patients/tokenId/${it.tokenId}?_method=PUT<%=languageInUrl %>"
							method="post" id="form_person">
							<!-- 						<form id="j_id_2s" name="j_id_2s" method="post" -->
							<!-- 							action="/index.xhtml" class="iseditable" -->
							<!-- 							enctype="application/x-www-form-urlencoded"> -->
							<div id="topFormPanel" class="panel panel-default">
								<div class="panel-heading">
									<h4><%=bundle.getString("patientData") %></h4>
								</div>
								<div class="panel-body">
									<jsp:include page="patientFormElements.jsp">
										<jsp:param value="edit" name="edit"/>
									</jsp:include>
								</div>
							</div>
							<div style="position: fixed; bottom: 0px; width: 760px;"
								id="stickyButtonBar" class="sticky_bar sticky_bottom">
								<div class="actions">
									<a href="#"
										onclick="document.getElementById('form_person').submit();"
										id="save" style="margin-right: 20px;"
										class="btn btn-success"> <i class="fa fa-check fa-fw"></i>
										<%=bundle.getString("save") %>
									</a><a href="#" onclick="window.history.back()" id="cancel"
										class="btn btn-danger"> <i
										class="fa fa-undo fa-fw"></i> <%=bundle.getString("cancel") %>
									</a>
								</div>
							</div>
						</form>
					</div>
				</div>
		</div>
	</div>
</body>
</html>